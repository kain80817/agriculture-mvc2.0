/*
 * @(#)ServiceAspect.java
 *
 * Copyright (c) 2015 FCYC Corporation.
 */
package cn.agriculture.common.aop;

import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 切面controller
 *
 * @author pengzhengming
 * @version 1.0 2015/05/25
 */
@Slf4j
@Component
@Aspect
public class ControllerAspect {

    /**
     * 必须为final String类型的,注解里要使用的变量只能是静态常量类型的
     */
    public static final String EDP = "execution(* cn.agriculture.web.controller..*(..))";

    /**
     * 环绕监控controller的方法
     * @param joinPoint 监控方法对象
     * @return 监控方法返回值
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    @Around(EDP)
    public Object logAround(ProceedingJoinPoint joinPoint) throws IllegalArgumentException, IllegalAccessException, Throwable {
        // 获取将要执行的方法名称
        String methodName = joinPoint.getSignature().getName();
        // 获取进入的类名
        String className = joinPoint.getSignature().getDeclaringTypeName();
        // 方法执行前的代理处理
        Object[] args = joinPoint.getArgs();
        log.info("className:" + className + ",methodName:" + methodName +"环绕开始");
        Object obj = null;
        try {
            obj = joinPoint.proceed(args);
            log.info("----------------------obj:" + obj);
            ServletRequestAttributes attrs =
                    (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpSession session = attrs.getRequest().getSession();
            String openid = (String) session.getAttribute("WEIXIN_OPENID");
            // 如果session中有微信的openid跳转到微信的页面
            if (!StringUtils.isEmpty(openid)) {
                String ret = String.valueOf(obj);
                if (!StringUtils.isEmpty(ret)) {
                    ret = ret.replace("shop/", "weixin/");
                    ret = ret.replace("mobile/", "weixin/");
                    log.info("---------------------------ret:" + ret);
                    return ret;
                }
            }
        } catch (Throwable e) {
            throw e; 
        } finally {
            // 方法执行后的代理处理
            log.info("className:" + className + ",methodName:" + methodName +"环绕结束");
        }
        return obj;
    }
}
