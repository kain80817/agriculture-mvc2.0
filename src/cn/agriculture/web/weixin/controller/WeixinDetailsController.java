package cn.agriculture.web.weixin.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.GuestForm;
import cn.agriculture.web.form.ReceiveForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.form.WeixinpayForm;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.GoodsService;
import cn.agriculture.web.service.GuestService;
import cn.agriculture.web.service.ReceiveService;
import cn.agriculture.web.weixin.service.WeixinCartService;
import cn.agriculture.web.weixin.service.WeixinGuestService;

@Slf4j
@Controller("WeixinDetailsController")
@RequestMapping("/")
public class WeixinDetailsController {
    @Autowired
    WeixinCartService weixinCartService;

    @Autowired
    CartService cartService;

    @Autowired
    ReceiveService receiveservice;

    @Autowired
    GoodsService goodsService;

    @Autowired
    WeixinGuestService weixinGuestService;

    @Autowired
    GuestService guestService;

    @RequestMapping(value = "submit", method = RequestMethod.POST,params="WeixinpaySubmit")
    public String executeAlipaySubmit(Model model, HttpSession session, GoodsForm goodsForm, CartForm cartForm, Device device) throws SQLException {
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for (int i=1; i<=sum; i++) {
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        String openId = (String)session.getAttribute("WEIXIN_OPENID");
        session.setAttribute("WEIXIN_GOODSFORM", goodsForm);
        session.setAttribute("WEIXIN_CARTFORM", cartForm);

        Boolean hasGuest = weixinGuestService.hasGuestInfo(openId);
        if (!hasGuest) {
            model.addAttribute("ret", "cart-2");
            // 依据微信openId可以找不到guestId，跳转到询问绑定
            return "weixin/bindIndex";
        }

        // 依据微信openId可以找到guestId
        GuestForm guest = weixinGuestService.getGuestInfo(openId);
        UVO uvo = new UVO();
        uvo.setGuestId(guest.getGuestId());
        uvo.setGuestName(guest.getGuestName());
        uvo.setPassword(guest.getPassword());
        uvo.setGender(guest.getGender());
        uvo.setEmail(guest.getEmail());
        uvo.setMobile(guest.getMobile());
        uvo.setQq(guest.getQq());
        uvo.setPhone(guest.getPhone());
        uvo.setZip(guest.getZip());
        uvo.setPoint(guest.getPoint());
        session.setAttribute("UVO", uvo);

        //非匿名购买
        cartForm.setGuestId(uvo.getGuestId());
        //设置updateTime
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cartForm.setUpdateTime(dateformat.format(date));
        //设置updateUser
        cartForm.setUpdateUser(uvo.getGuestName());

        List<CartForm> cartFormList = cartService.searchCartList(cartForm);

        ReceiveForm receiveForm = new ReceiveForm();
        receiveForm.setGuestId(uvo.getGuestId());
        GuestForm guestForm = new GuestForm();
        guestForm.setGuestId(uvo.getGuestId());
        String addressDefault = guestService.searchAddressId(guestForm).getAddressId();
        model.addAttribute("addressDefault", addressDefault);
        List<ReceiveForm> list = receiveservice.searchlist(receiveForm);
        model.addAttribute("list", list);
        WeixinpayForm weixinPayForm = new WeixinpayForm();
        weixinPayForm = weixinCartService.searchWeixinPayImmediately(cartForm);
          model.addAttribute("alipayForm",weixinPayForm);
        if (weixinPayForm == null) {
            model.addAttribute("commodityType", commodityType);
            model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));
            model.addAttribute("cartList", cartFormList);
            model.addAttribute("message", "库存不够！");
            model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
            if ("201500000000030".equals(goodsForm.getCommodityTypeId())) {
                return "weixin/pointExchange/exchangeDetails";
            } else {
                return "weixin/goods/details";
            }
        }
        // 登陆后并且库存充足的场合，判定用户积分是否充足。
        String sumPoint = weixinPayForm.getSumPoint(); // 商品总积分
        // 用户剩余积分查询
        String guestPoint = guestService.searchGuestPoint(guestForm).getGuestPoint();
        // 换购商品积分  > 用户积分 的场合 ，并且积分兑换的场合
        if ("201500000000030".equals(goodsForm.getCommodityTypeId())
                && Double.valueOf(sumPoint) > Double.valueOf(guestPoint)) {
              model.addAttribute("commodityType", commodityType);
              model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));
              model.addAttribute("cartList", cartFormList);
              model.addAttribute("message", "用户积分不足！");
              model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
              return "weixin/pointExchange/exchangeDetails";
        }

        model.addAttribute("cartList", cartFormList);
        return "weixin/cart/cart-2";
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST,params="WeixinAddCart")
    public String executeAddCart(Model model, HttpSession session, GoodsForm goodsFormInfo, CartForm cartForm, Device device) throws SQLException {
        log.info("追加购物车");
        GoodsForm goodsForm = new GoodsForm();
        goodsForm.setCommodityId(cartForm.getCommodityId());
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("commodityType",commodityType);
        model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));

        String openId = (String)session.getAttribute("WEIXIN_OPENID");
        session.setAttribute("WEIXIN_GOODSFORM", goodsFormInfo);
        session.setAttribute("WEIXIN_CARTFORM", cartForm);
        Boolean hasGuest = weixinGuestService.hasGuestInfo(openId);
        if (!hasGuest) {
            model.addAttribute("ret", "cart-1");
            // 依据微信openId可以找不到guestId，跳转到询问绑定
            return "weixin/bindIndex";
        }
        // 依据微信openId可以找到guestId
        GuestForm guest = weixinGuestService.getGuestInfo(openId);
        UVO uvo = new UVO();
        uvo.setGuestId(guest.getGuestId());
        uvo.setGuestName(guest.getGuestName());
        uvo.setPassword(guest.getPassword());
        uvo.setGender(guest.getGender());
        uvo.setEmail(guest.getEmail());
        uvo.setMobile(guest.getMobile());
        uvo.setQq(guest.getQq());
        uvo.setPhone(guest.getPhone());
        uvo.setZip(guest.getZip());
        uvo.setPoint(guest.getPoint());
        session.setAttribute("UVO", uvo);

        cartForm.setUpdateUser(guest.getGuestName());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cartForm.setUpdateTime(dateformat.format(date));
        cartForm.setGuestId(guest.getGuestId());
        WeixinpayForm weixinpayForm = new WeixinpayForm();
        weixinpayForm = weixinCartService.searchWeixinPay(cartForm);
        if (weixinpayForm == null) {
            model.addAttribute("cartList", cartService.searchCartList(cartForm));
            model.addAttribute("message", "库存不够！");
            model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
            if ("201500000000030".equals(goodsFormInfo.getCommodityTypeId())) {
                return "weixin/pointExchange/exchangeDetails";
            } else {
                return "weixin/goods/details";
            }
        }

        boolean result = cartService.addCart(cartForm);
        if (!result) {
            throw new SQLException("追加购物车失败！");
        }
        List<CartForm> cartFormList = cartService.searchCartList(cartForm);
        for(int i=0;i<cartFormList.size();i++){
            BigDecimal smallSumPrice = new BigDecimal(Double.toString(Double.valueOf(cartFormList.get(i).getCount())*Double.valueOf(cartFormList.get(i).getRetailPrice())));
            cartFormList.get(i).setSmallSumPrice(String.valueOf(smallSumPrice.setScale(2, BigDecimal.ROUND_HALF_UP)));
        }
        model.addAttribute("cartList", cartFormList);
        return "weixin/cart/cart-1";
    }


    @RequestMapping(value = "initWeixinLogin", method = RequestMethod.GET)
    public String initLogin(Model model, String ret) {
        log.info("微信客户绑定界面初始化");
        GoodsForm goodsForm = new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        List<CartForm> cartList = new ArrayList<>();
        model.addAttribute("cartList", cartList);
        GuestForm guestForm = new GuestForm();
        model.addAttribute("guestForm", guestForm);
        model.addAttribute("ret", ret);
        return "weixin/login";
    }

    @RequestMapping(value = "initWeixinRegister", method = RequestMethod.GET)
    public String initRegister(Model model, String ret) {
        log.info("微信客户注册界面初始化");
        GoodsForm goodsForm = new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType", commodityType);
        List<CartForm> cartList = new ArrayList<>();
        model.addAttribute("cartList", cartList);
        GuestForm guestForm = new GuestForm();
        model.addAttribute("guestForm", guestForm);
        model.addAttribute("ret", ret);
        return "weixin/register-1";
    }

    @RequestMapping(value = "initWeixinGuest", method = RequestMethod.GET)
    public String initGuest(Model model, HttpSession session) {
        log.info("微信匿名购买商品销售页面初始化。");
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        GuestForm guest = new GuestForm();
        guest.setGuestId("Guest" + dateformat.format(date));
        GoodsForm goodsFormInfo = (GoodsForm) session.getAttribute("WEIXIN_GOODSFORM");
        CartForm cartForm = (CartForm) session.getAttribute("WEIXIN_CARTFORM");

        GoodsForm goodsForm = new GoodsForm();
        goodsForm.setCommodityId(cartForm.getCommodityId());
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("commodityType",commodityType);
        model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));

        WeixinpayForm weixinPayForm = new WeixinpayForm();
        cartForm.setGuestId(guest.getGuestId());
        weixinPayForm = weixinCartService.searchWeixinPay(cartForm);
        List<CartForm> cartList = new ArrayList<>();
        model.addAttribute("cartList", cartList);
        if (weixinPayForm == null) {
            model.addAttribute("message", "库存不够！");
            model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
            if ("201500000000030".equals(goodsFormInfo.getCommodityTypeId())) {
                return "weixin/pointExchange/exchangeDetails";
            } else {
                return "weixin/goods/details";
            }
        }
        model.addAttribute("weixinPayForm", weixinPayForm);
        return "weixin/weixinPay/guestWeixinPayConfirm";
    }
}
