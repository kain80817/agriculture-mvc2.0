package cn.agriculture.web.weixin.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import weixin.popular.api.SnsAPI;
import weixin.popular.api.TokenAPI;
import weixin.popular.api.UserAPI;
import weixin.popular.bean.SnsToken;
import weixin.popular.bean.Token;
import weixin.popular.bean.User;
import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.GoodsService;

@Slf4j
@Controller("WeixinAuthorizeController")
@RequestMapping("/")
@PropertySource("classpath:system.properties")
public class WeixinAuthorizeController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private CartService cartService;

    @Autowired
    private Environment env;

    @RequestMapping(value = "initWeixin", method = RequestMethod.GET)
    public String init(Model model, String code, String state) {
        log.info("微信网页授权初始化");
        ServletRequestAttributes attrs =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpSession session = attrs.getRequest().getSession();

        String ret = getWinxinUser(code, session);
        if (!StringUtils.isEmpty(ret)) {
            return ret;
        }
        UVO uvo = new UVO();
        session.setAttribute("UVO", uvo);
        CartForm cartForm = new CartForm();
        cartForm.setGuestId(uvo.getGuestId());
        model.addAttribute("cartList", cartService.searchCartList(cartForm));
        GoodsForm goodsForm = new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();

        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setCount(i+"F");
            commodityType.get(i-1).setCss("columnT"+" "+"columnT-"+(i%6+1));
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        model.addAttribute("list", goodsService.searchGoodsList(goodsForm));
        GoodsForm goodsFormForId = new GoodsForm();
        for(int i=0;i<commodityType.size();i++){
            goodsFormForId.setCommodityTypeId(commodityType.get(i).getCommodityTypeId());
            commodityType.get(i).setList(goodsService.searchGoodsListLimit(goodsFormForId));
        }
        return "weixin/index";
    }

    private String getWinxinUser(String code, HttpSession session) {
        String appid = env.getProperty("weixin.appid");
        String secret = env.getProperty("weixin.secret");
        // 通过code换取网页授权access_token
        SnsToken snsToken = SnsAPI.oauth2AccessToken(appid, secret, code);
        if (!StringUtils.isEmpty(snsToken.getErrcode())) {
            log.info("SnsAPI.oauth2AccessToken 失败 Errcode：" + snsToken.getErrcode() + ",errmsg:" + snsToken.getErrmsg());
            return "weixin/error";
        }
        String openId = snsToken.getOpenid();
        // 取得基础access_token
        Token token = TokenAPI.token(appid, secret);
        if (!StringUtils.isEmpty(token.getErrcode())) {
            log.info("TokenAPI.token 失败 Errcode：" + token.getErrcode() + ",errmsg:" + token.getErrmsg());
            return "weixin/error";
        }
        String accessToken = token.getAccess_token();
        // 获取用户基本信息
        User user = UserAPI.userInfo(accessToken, openId);
        if (!StringUtils.isEmpty(user.getErrcode())) {
            log.info("UserAPI.userInfo 失败 Errcode：" + user.getErrcode() + ",errmsg:" + user.getErrmsg());
            return "weixin/error";
        }
        session.setAttribute("WEIXIN_USER", user);
        session.setAttribute("WEIXIN_OPENID", openId);
        return null;
    }
}
