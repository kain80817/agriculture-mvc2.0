package cn.agriculture.web.weixin.service;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import cn.agriculture.common.util.MD5Util;
import cn.agriculture.web.form.GuestForm;

@Service
@PropertySource("classpath:system.properties")
public class WeixinGuestService {

    @Autowired
    QueryDAO queryDao;

    @Autowired
    UpdateDAO updateDao;


    public boolean hasGuestInfo(String openId) {
        GuestForm frm = new GuestForm();
        frm.setOpenId(openId);
        Integer count = queryDao.executeForObject("WeixinGuest.selectCountGuestByOpenId", frm, Integer.class);
        if (count.intValue() <= 0) {
            return false;
        }
        return true;
    }

    public GuestForm getGuestInfo(String openId) {
        GuestForm frm = new GuestForm();
        frm.setOpenId(openId);
        return queryDao.executeForObject("WeixinGuest.selectGuestByOpenId", frm, GuestForm.class);
    }

    public boolean addGuest(GuestForm frm) {
        GuestForm guestForm = new GuestForm();
        guestForm.setEmail(frm.getEmail());
        guestForm.setGender(frm.getGender());
        guestForm.setGuestId(frm.getGuestId());
        guestForm.setGuestName(frm.getGuestName());
        guestForm.setMobile(frm.getMobile());
        guestForm.setPassword(MD5Util.getMD5(frm.getPassword()));
        guestForm.setPhone(frm.getPhone());
        guestForm.setQq(frm.getQq());
        guestForm.setUpdateTime(frm.getUpdateTime());
        guestForm.setUpdateUser(frm.getUpdateUser());
        guestForm.setZip(frm.getZip());
        guestForm.setPoint(frm.getPoint());
        guestForm.setOpenId(frm.getOpenId());
        try {
            int result = updateDao.execute("WeixinGuest.addGuest", guestForm);
            if (result == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateGuestOpenId(GuestForm guestForm) {
        try {
            int result = updateDao.execute("WeixinGuest.updateGuest", guestForm);
            if (result == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
