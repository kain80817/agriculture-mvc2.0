package cn.agriculture.web.service;

import java.util.List;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.agriculture.common.util.UUIDUtil;
import cn.agriculture.web.form.IntegralRuleForm;

@Service
public class IntegralRuleService {

    @Autowired
    QueryDAO queryDao;

    @Autowired
    UpdateDAO updateDao;

    /**
     * 查询积分规则
     * @param frm 条件
     * @return
     */
    public List<IntegralRuleForm> searchIntegralRuleList(IntegralRuleForm frm) {
        List<IntegralRuleForm> result = queryDao.executeForObjectList("IntegralRule.select", frm);
        return result;
    }

    /**
     * 查询积分规则
     * @param frm 条件
     * @return
     */
    public boolean add(IntegralRuleForm frm) {
        frm.setId(UUIDUtil.generateShortUuid());
        if (StringUtils.isEmpty(frm.getHowMoney())) {
            frm.setHowMoney("1");
        }
        int result = updateDao.execute("IntegralRule.add", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

    /**
     * 查询积分规则
     * @param frm 条件
     * @return
     */
    public boolean update(IntegralRuleForm frm) {
        if (StringUtils.isEmpty(frm.getHowMoney())) {
            frm.setHowMoney("1");
        }
        int result = updateDao.execute("IntegralRule.update", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

    public boolean delete(IntegralRuleForm frm) {
        int result = updateDao.execute("IntegralRule.delete", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }
}
