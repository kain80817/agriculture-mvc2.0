package cn.agriculture.web.service;

import java.util.List;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.agriculture.web.form.IntegralExchangeReportForm;

@Service
public class IntegralExchangeReportService {

    @Autowired
    QueryDAO queryDao;

    @Autowired
    UpdateDAO updateDao;

    public List<IntegralExchangeReportForm> search() {
        List<IntegralExchangeReportForm> result = queryDao.executeForObjectList("AlipayReport.selectIntegralExchangeReportList", null);
        return result;
    }

    public boolean del(IntegralExchangeReportForm frm) {

        int result = updateDao.execute("AlipayReport.deleteAlipayReport", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

    public boolean deliver(IntegralExchangeReportForm frm) {

        int result = updateDao.execute("AlipayReport.deliverIntegralExchangeReport", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

}
