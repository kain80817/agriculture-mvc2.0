package cn.agriculture.web.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.CommodityTypeService;
import cn.agriculture.web.service.GoodsService;
import cn.agriculture.web.service.GuestService;

@Slf4j
@Controller("PointExchangeController")
@RequestMapping("/")
public class PointExchangeController {

	@Autowired
	GuestService guestService;
	
	@Autowired
	CommodityTypeService commodityTypeService;
	
	@Autowired
	GoodsService goodsService;
	
	@Autowired
	CartService cartService;
	
    @RequestMapping(value = "pointExchangeList", method = RequestMethod.GET)
    public String initGoods(Model model, HttpSession session, GoodsForm goodsForm, Device device) throws UnsupportedEncodingException {
        log.info("商品列表初始化");
        goodsForm.setCommodityTypeId("201500000000030");
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        
        if(goodsForm.getCommodityTypeId()==null)
        {
            model.addAttribute("list", goodsService.getTypeList(goodsForm));
            model.addAttribute("goodsForm", goodsForm);
        }
        else
            {model.addAttribute("goodsForm", goodsForm);
            model.addAttribute("list", goodsService.getTypeList(goodsForm));
            }
        UVO uvo = (UVO)session.getAttribute("UVO");
        if (uvo == null) {
            uvo = new UVO();
            session.setAttribute("UVO", uvo);
        }
        model.addAttribute("orderTypeId", 1);
        CartForm cartForm = new CartForm();
        cartForm.setGuestId(uvo.getGuestId());
        model.addAttribute("cartList", cartService.searchCartList(cartForm));
        if(device.isNormal()) {
            return "shop/pointExchange/exchangeList";
        } else {
            return "mobile/pointExchange/exchangeList";
        }
    }
}
