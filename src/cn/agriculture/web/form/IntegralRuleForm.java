package cn.agriculture.web.form;

import cn.agriculture.common.validator.constraints.NotEmpty;
import lombok.Data;

@Data
public class IntegralRuleForm {
    private String id;
    @NotEmpty(field="规则名称",  message="{errors.required}")
    private String name;
    @NotEmpty(field="规则类型",  message="{errors.required}")
    private String type;
    @NotEmpty(field="规则开始时间",  message="{errors.required}")
    private String startDate;
    @NotEmpty(field="规则结束时间",  message="{errors.required}")
    private String endDate;
    @NotEmpty(field="规则状态",  message="{errors.required}")
    private String status;
    @NotEmpty(field="规则",  message="{errors.required}")
    private String howMoney;
    @NotEmpty(field="规则",  message="{errors.required}")
    private String howScore;
    private String howSet;
    private String updateTime;
    private String updateUser;
}
