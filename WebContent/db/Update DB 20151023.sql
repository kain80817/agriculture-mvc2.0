alter table guest add open_id varchar(45) DEFAULT NULL COMMENT '微信openId';

DROP TABLE IF EXISTS `weixin_history`;
CREATE TABLE `weixin_history` (
  `out_trade_no` varchar(45) NOT NULL,
  `subject` varchar(450) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  `body` varchar(450) DEFAULT NULL,
  `show_url` varchar(450) DEFAULT NULL,
  `receive_name` varchar(45) DEFAULT NULL,
  `receive_address` varchar(450) DEFAULT NULL,
  `receive_zip` varchar(45) DEFAULT NULL,
  `receive_phone` varchar(45) DEFAULT NULL,
  `receive_mobile` varchar(45) DEFAULT NULL,
  `guest_id` varchar(45) DEFAULT NULL,
  `update_time` datetime NOT NULL,
  `update_user` varchar(45) NOT NULL,
  `is_rebate` varchar(45) DEFAULT NULL,
  `commodity_id` varchar(45) DEFAULT NULL,
  `sumPoint` varchar(45) DEFAULT NULL COMMENT '商品总积分',
  `state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`out_trade_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;