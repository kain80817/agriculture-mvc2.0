﻿alter table alipay_history add sumPoint varchar(45) DEFAULT NULL COMMENT '商品总积分';
alter table alipay_history add is_deliver varchar(45) DEFAULT NULL COMMENT '是否发货';
alter table commodity add point varchar(45) DEFAULT NULL COMMENT '积分兑换扣减积分';
alter table guest add point varchar(45) DEFAULT NULL COMMENT '剩余积分';

DROP TABLE IF EXISTS `integral_rule`;
CREATE TABLE `integral_rule` (
  `integral_rule_id` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `howMoney` int(9) DEFAULT NULL,
  `howScore` int(9) DEFAULT NULL,
  `howSet` varchar(45) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`integral_rule_id`)
);